<?php
/**
 * The template for displaying Author Archive pages
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
global $wp_query;

$context = Timber::get_context();
$context['posts'] = Timber::get_posts();
$context['activities'] = [];
foreach ($context['posts'] as $post){
	$post->creation_date = $post->post_date;
	array_push($context['activities'], $post);
}
if ( isset( $wp_query->query_vars['author'] ) ) {
	$author = new TimberUser( $wp_query->query_vars['author'] );
	$context['author'] = $author;
	$context['title'] = 'Author Archives: ' . $author->name();
	$context['comments'] = get_comments([
		'user_id' => $author->id(),
		'status' => 'approve'
	]);
	foreach ($context['comments'] as $comment) {
		$comment->creation_date = $comment->comment_date;
		array_push($context['activities'], $comment);
	}
}

function date_sort($a, $b)
{
	if ( $a->creation_date > $b->creation_date ) return -1;
	if ( $a->creation_date < $b->creation_date ) return 1;
	return 0;
}
if($context['activities'] != null){
	uasort($context['activities'], 'date_sort');
}

Timber::render( array( 'author.twig', 'archive.twig' ), $context );
