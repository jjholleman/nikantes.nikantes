<?php

$context = Timber::get_context();
$context['posts'] = Timber::get_posts();

$context['trainingstijden']['dinsdag'] = get_field('training_day_tuesday','option');
$context['trainingstijden']['dinsdag']['day_name'] = "Dinsdag";
$context['trainingstijden']['donderdag'] = get_field('training_day_thursday','option');
$context['trainingstijden']['donderdag']['day_name'] = "Donderdag";
$context['trainingstijden_options'] = [
	'toggle_trainings' => get_field('toggle_trainings', 'option'),
	'all_trainings_disabled_announcement' => get_field('all_trainings_disabled_announcement', 'option'),
	'adjustments' => get_field('adjustments', 'option'),
	'adjustments_enabled' => get_field('adjustments_enabled', 'option'),
];

$acf_agenda = get_field('agenda_events', 'option');
function date_sort($a, $b)
{
	if ( $a['date'] < $b['date'] ) return -1;
	if ( $a['date'] > $b['date'] ) return 1;
	return 0;
}
if($acf_agenda != null){
	uasort($acf_agenda, 'date_sort');
}
$context['agenda'] = $acf_agenda;
$sponsors = get_field('sponsors', 'option');
if($sponsors) {
    shuffle($sponsors);
}
$context['sponsors'] = $sponsors;

$context['copyright_year'] = date("Y");

$context['mini_blokken'] = get_field('mini_blokken', 'option');

$templates = array( 'index.twig' );
if ( is_home() ) {
	array_unshift( $templates, 'home.twig' );
}
Timber::render( $templates, $context );