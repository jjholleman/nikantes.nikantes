<?php
/**
 * Template Name: Teams Overzicht
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['teams'] = [
	"senioren" => get_field('teams_senioren'),
	"a" => get_field('teams_A'),
	"b" => get_field('teams_B'),
	"c" => get_field('teams_C'),
	"d" => get_field('teams_D'),
	"e" => get_field('teams_E'),
	"f" => get_field('teams_F'),
	"g" => get_field('teams_G'),
	"k" => get_field('teams_kangoeroes')
];





Timber::render( array( 'custom/page-teams.twig', 'page.twig' ), $context );