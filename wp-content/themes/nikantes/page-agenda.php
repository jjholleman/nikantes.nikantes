<?php
/**
 * Template Name: Jaarplanning Overzicht
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$acf_agenda = get_field('agenda_events', 'option');
function date_sort($a, $b)
{
	if ( $a['date'] < $b['date'] ) return -1;
	if ( $a['date'] > $b['date'] ) return 1;
	return 0;
}
if($acf_agenda != null){
	uasort($acf_agenda, 'date_sort');
}
$context['agenda'] = $acf_agenda;

Timber::render( array( 'custom/page-agenda.twig', 'page.twig' ), $context );