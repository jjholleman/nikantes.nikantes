<?php
/**
 * Template Name: Contributie Overzicht
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

Timber::render( array( 'custom/page-contributie.twig', 'page.twig' ), $context );