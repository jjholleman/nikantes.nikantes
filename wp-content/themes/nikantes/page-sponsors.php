<?php
/**
 * Template Name: Sponsors Overzicht
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['sponsors'] = get_field('sponsors', 'option');




Timber::render( array( 'custom/page-sponsors.twig', 'page.twig' ), $context );