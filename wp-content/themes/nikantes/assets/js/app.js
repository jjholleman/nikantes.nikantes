$(function (jQuery) {
    var slideout = new Slideout({
        'panel': document.getElementById('panel'),
        'menu': document.getElementById('nav-mobile-menu'),
        'padding': 256,
        'tolerance': 70,
        'easing': 'cubic-bezier(.32,2,.55,.27)'
    });

    $(".qn--trainings").on('click', function () {
        if(window.location.pathname !== "/") {
            window.location.href = "/trainingstijden"
        } else {
            $(".qi__content").slideToggle("400", "swing");
        }
    });

    $(".nav-toggle").on('click', function () {
        slideout.toggle();
    });
    function close(eve) {
        eve.preventDefault();
        slideout.close();
    }

    slideout
        .on('beforeopen', function () {
            this.panel.classList.add('panel-open');
            $(".nav-toggle-wrapper").addClass('nav-mobile-open');
        })
        .on('open', function () {
            this.panel.addEventListener('click', close);
        })
        .on('beforeclose', function () {
            this.panel.classList.remove('panel-open');
            this.panel.removeEventListener('click', close);
            $(".nav-toggle-wrapper").removeClass('nav-mobile-open');
        });

    $(document).ready(function($){
        console.log("document ready")
    });
    $(window).load(function(){
        console.log("window load");
    });
});