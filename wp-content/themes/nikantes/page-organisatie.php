<?php
/**
 * Template Name: Organisatie Overzicht
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$organisaties = get_field('organisatie', 'option');
$context['organisaties'] = $organisaties;


Timber::render( array( 'custom/page-organisatie.twig', 'page.twig' ), $context );