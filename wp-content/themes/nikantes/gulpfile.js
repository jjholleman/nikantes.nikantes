const { src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass')(require('sass'));

const scss_dir = './assets/sass/**/*.scss';

function compileSass() {
    return src(scss_dir)
        .pipe(sass().on('error', sass.logError))
        .pipe(dest('.'));
}

function watchSass() {
    watch(scss_dir, compileSass)
        .on('change', function(event) {
            console.log('File ' + event + ' was changed, running tasks...');
        });
}

exports.sass = compileSass;
exports['sass:watch'] = series(compileSass, watchSass);
