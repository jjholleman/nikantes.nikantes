<?php
/**
 * Template Name: Trainers Overzicht
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

Timber::render( array( 'custom/page-trainers.twig', 'page.twig' ), $context );