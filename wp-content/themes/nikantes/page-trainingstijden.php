<?php
/**
 * Template Name: Trainingstijden Overzicht
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$context['trainingstijden']['dinsdag'] = get_field('training_day_tuesday','option');
$context['trainingstijden']['dinsdag']['day_name'] = "Dinsdag";
$context['trainingstijden']['donderdag'] = get_field('training_day_thursday','option');
$context['trainingstijden']['donderdag']['day_name'] = "Donderdag";
$context['trainingstijden_options'] = [
	'toggle_trainings' => get_field('toggle_trainings', 'option'),
	'all_trainings_disabled_announcement' => get_field('all_trainings_disabled_announcement', 'option'),
	'adjustments' => get_field('adjustments', 'option'),
	'adjustments_enabled' => get_field('adjustments_enabled', 'option'),
];

Timber::render( array( 'custom/page-trainingstijden.twig', 'page.twig' ), $context );