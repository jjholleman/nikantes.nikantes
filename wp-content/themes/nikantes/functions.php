<?php
$timber = new \Timber\Timber();


/**
 * Activate ACF
 */
function wpa__prelicense()
{
    if (function_exists('acf_pro_is_license_active') && !acf_pro_is_license_active()) {
        $args = array(
            '_nonce' => wp_create_nonce('activate_pro_licence'),
            'acf_license' => base64_encode('order_id=37918|type=personal|date=2014-08-21 15:02:59'),
            'acf_version' => acf_get_setting('version'),
            'wp_name' => get_bloginfo('name'),
            'wp_url' => home_url(),
            'wp_version' => get_bloginfo('version'),
            'wp_language' => get_bloginfo('language'),
            'wp_timezone' => get_option('timezone_string'),
        );
        $response = acf_pro_get_remote_response('activate-license', $args);
        $response = json_decode($response, true);
        if ($response['status'] == 1) {
            acf_pro_update_license($response['license']);
        }
    }
}

add_action('admin_init', 'wpa__prelicense', 99);

/**
 * Timber stuff
 */
if (!class_exists('Timber')) {
    add_action('admin_notices', function () {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
    });

    add_filter('template_include', function ($template) {
        return get_stylesheet_directory() . '/static/no-timber.html';
    });

    return;
}

if ( function_exists( 'acf_add_options_page' ) ) {

    $parent = acf_add_options_page( [
        'page_title' => 'Extra Website Instellingen',
        'menu_title' => 'Extra Website Instellingen',
        'menu_slug'  => 'extra-website-settings',
        'redirect'   => 'false'
    ] );
    acf_add_options_sub_page( [
        'page_title'  => 'Agenda',
        'menu_title'  => 'Agenda',
        'parent_slug' => $parent['menu_slug'],
    ] );
    acf_add_options_sub_page( [
        'page_title'  => 'Trainingstijden',
        'menu_title'  => 'Trainingstijden',
        'parent_slug' => $parent['menu_slug'],
    ] );
    acf_add_options_sub_page( [
        'page_title'  => 'Populaire Items',
        'menu_title'  => 'Populaire Items',
        'parent_slug' => $parent['menu_slug'],
    ] );
    acf_add_options_sub_page( [
        'page_title'  => 'Sponsoren',
        'menu_title'  => 'Sponsoren',
        'parent_slug' => $parent['menu_slug'],
    ] );
    acf_add_options_sub_page( [
        'page_title'  => 'Organisatie',
        'menu_title'  => 'Organisatie',
        'parent_slug' => $parent['menu_slug'],
    ] );
    acf_add_options_sub_page( [
        'page_title'  => 'Activiteiten',
        'menu_title'  => 'Activiteiten',
        'parent_slug' => $parent['menu_slug'],
    ] );
    acf_add_options_sub_page( [
        'page_title'  => 'Mini-blokken',
        'menu_title'  => 'Mini-blokken',
        'parent_slug' => $parent['menu_slug'],
    ] );
    acf_add_options_sub_page( [
        'page_title'  => 'Over Nikantes',
        'menu_title'  => 'Over Nikantes',
        'parent_slug' => $parent['menu_slug'],
    ] );
}

/**
 * Add admin permissions
 */
if (is_admin() && '1' == isset($_GET['reload_caps'])) {
    // add editor the privilege to edit theme
    // get the the role object
    $editor_role = get_role('editor');
    $admin_role = get_role('administrator');

    // add $cap capability to this role object
    $editor_role->add_cap('manage_options');
    $editor_role->add_cap('edit_theme_options');

    $admin_role->add_cap('manage_options');
    $admin_role->add_cap('editor');

    echo "capabilities added";
}


Timber::$dirname = array('templates', 'views');

/**
 * Custom Functions
 *
 */

function is_elementor()
{
    global $post;
    if ((\Elementor\Plugin::$instance->db->is_built_with_elementor($post->ID)) === true) {
        return "elementor";
    } else {
        return false;
    }
}

class StarterSite extends TimberSite
{

    function __construct()
    {
        add_theme_support('post-formats');
        add_theme_support('post-thumbnails');
        add_theme_support('menus');
        add_filter('timber_context', array($this, 'add_to_context'));
        add_filter('get_twig', array($this, 'add_to_twig'));
        add_action('init', array($this, 'register_post_types'));
        add_action('init', array($this, 'register_taxonomies'));
        add_filter('use_default_gallery_style', '__return_false');

        parent::__construct();
    }

    function register_post_types()
    {
        //this is where you can register custom post types
        add_theme_support('post-formats', array('link'));
    }

    function register_taxonomies()
    {
        //this is where you can register custom taxonomies
    }

    function add_to_context($context)
    {
        $context['menu'] = new TimberMenu();
        $context['site'] = $this;
        $stringsJson = file_get_contents("strings.json", FILE_USE_INCLUDE_PATH);
        $json_a = json_decode($stringsJson, true);
        $context['strings'] = $json_a;

        return $context;
    }

    function add_to_twig($twig)
    {
        /* this is where you can add your own functions to twig */
        $twig->addExtension(new Twig_Extension_StringLoader());
        $twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));

        return $twig;
    }


}

new StarterSite();